use std::f64;
use wasm_bindgen::prelude::*;
use wasm_bindgen::JsCast;
use wasm_bindgen::JsValue;

use crate::letters;

#[allow(unused_parens)]
pub fn seed_to_color(seed: String) -> String {
  let seed_vec: Vec<char> = seed.chars().collect();
  let first_char = seed_vec[0];
  if (first_char > 'p') {
    return "#0ff".to_string(); // cyan
  } else if (first_char > 'a') {
    return "#f0f".to_string(); // magenta
  } else {
    return "#ff0".to_string(); // yellow
  }
}

#[allow(unused_parens)]
pub fn seed_to_int(seed: String) -> u8 {
  let seed_vec: Vec<char> = seed.chars().collect();
  let second_char = seed_vec[1];
  if (second_char > 'g') {
    return 0; // usually 0, so usually circle
  } else if (second_char > '8') {
    return 1;
  } else {
    return 2;
  }
}

pub fn draw_small_circle(
  ctx: web_sys::CanvasRenderingContext2d, vw: f64, vh: f64, x: f64, y: f64
) {
  ctx.move_to(vw * x, vh * y);
  ctx
    .arc(vw * (x - 0.01), vh * y, vw * 0.01, 0.0, f64::consts::PI * 2.0)
    .unwrap();
}

pub fn draw_small_star(
  ctx: web_sys::CanvasRenderingContext2d, vw: f64, vh: f64, x: f64, y: f64
) {
  ctx.move_to(vw * (x - 0.02), vh * y);
  ctx.line_to(vw * (x + 0.02), vh * y);
  ctx.move_to(vw * (x - 0.01), vh * (y - 0.01));
  ctx.line_to(vw * (x + 0.01), vh * (y + 0.01));
  ctx.move_to(vw * (x + 0.01), vh * (y - 0.01));
  ctx.line_to(vw * (x - 0.01), vh * (y + 0.01));
}

pub fn draw_small_triangle(
  ctx: web_sys::CanvasRenderingContext2d, vw: f64, vh: f64, x: f64, y: f64
) {
  ctx.move_to(vw * x, vh * y);
  ctx.line_to(vw * (x + 0.02), vh * (y + 0.005));
  ctx.line_to(vw * (x + 0.01), vh * (y + 0.01));
  ctx.line_to(vw * x, vh * y);
}

#[allow(unused_parens)]
pub fn draw_letter(
  ctx: web_sys::CanvasRenderingContext2d,
  vw: f64, vh: f64, ox: f64, oy: f64, scale: f64, dx: f64, dy: f64, letter: Vec<(f64, f64)>, rnd_int: u8
) {
  let dx = dx * scale + ox;
  let dy = dy * scale + oy;
  for point in letter.iter() {
    let x = point.0 * scale + dx;
    let y = point.1 * scale + dy;
    if (rnd_int == 0) {
      draw_small_circle(ctx.clone(), vw, vh, x, y);
    } else if (rnd_int == 1) {
      draw_small_star(ctx.clone(), vw, vh, x, y);
    } else {
      draw_small_triangle(ctx.clone(), vw, vh, x, y);
    }
  }
}

#[wasm_bindgen]
#[allow(unused_parens)]
pub fn draw_message(vw: f64, vh: f64, canvas_id: String, start: f64, now: f64, seed: String) {
  // Convert seed and cycle position to various parameters.
  let cycle_position = (now - start) % 10000.0;
  let base_color = seed_to_color(seed.clone());
  let rnd_int = seed_to_int(seed);
  let line_width = (10000.0 - cycle_position) / 5000.0; // thicker, earlier

  // Get a reference to the canvas’s 2D drawing context.
  let document = web_sys::window().unwrap().document().unwrap();
  let canvas = document.get_element_by_id(&canvas_id).unwrap();
  let canvas: web_sys::HtmlCanvasElement = canvas
    .dyn_into::<web_sys::HtmlCanvasElement>()
    .map_err(|_| ())
    .unwrap();
  let ctx = canvas
    .get_context("2d")
    .unwrap()
    .unwrap()
    .dyn_into::<web_sys::CanvasRenderingContext2d>()
    .unwrap();
  ctx.begin_path();
  ctx.set_stroke_style(&JsValue::from_str(&base_color));
  ctx.set_line_width(line_width);
  ctx.clear_rect(0.0, 0.0, vw, vh);

  // Set the scale of all letters.
  let scale = cycle_position / 40000.0;

  // Set the origin-coordinate of all letters.
  let ox = 0.5;
  let oy = 1.0 - (cycle_position / 10000.0);

  // Draw MANY.
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 1.2 - 3.5, 0.2 - 4.0, letters::M.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 2.4 - 3.5, 0.2 - 4.0, letters::A.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 3.6 - 3.5, 0.2 - 4.0, letters::N.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 4.8 - 3.5, 0.2 - 4.0, letters::Y.to_vec(), rnd_int);

  // Draw HAPPY.
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 0.7 - 3.5, 1.7 - 4.0, letters::H.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 1.9 - 3.5, 1.7 - 4.0, letters::A.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 3.1 - 3.5, 1.7 - 4.0, letters::P.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 4.3 - 3.5, 1.7 - 4.0, letters::P.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 5.5 - 3.5, 1.7 - 4.0, letters::Y.to_vec(), rnd_int);

  // Draw RETURNS.
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 0.2 - 3.5, 3.2 - 4.0, letters::R.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 1.4 - 3.5, 3.2 - 4.0, letters::E.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 2.35 - 3.5, 3.2 - 4.0, letters::T.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 3.3 - 3.5, 3.2 - 4.0, letters::U.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 4.4 - 3.5, 3.2 - 4.0, letters::R.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 5.5 - 3.5, 3.2 - 4.0, letters::N.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 6.7 - 3.5, 3.2 - 4.0, letters::S.to_vec(), rnd_int);

  // Draw MARI!
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 1.2 - 3.5, 4.7 - 4.0, letters::M.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 2.4 - 3.5, 4.7 - 4.0, letters::A.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 3.6 - 3.5, 4.7 - 4.0, letters::R.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 4.7 - 3.5, 4.7 - 4.0, letters::I.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 5.7 - 3.5, 4.7 - 4.0, letters::EXC.to_vec(), rnd_int);

  // Finish the main letterforms.
  ctx.stroke();

  // Add white highlight, early in the cucle position.
  ctx.set_stroke_style(&JsValue::from_str(&"#fff"));
  ctx.set_line_width(line_width / 4.0);

  // Draw MANY.
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 1.2 - 3.5, 0.2 - 4.0, letters::M.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 2.4 - 3.5, 0.2 - 4.0, letters::A.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 3.6 - 3.5, 0.2 - 4.0, letters::N.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 4.8 - 3.5, 0.2 - 4.0, letters::Y.to_vec(), rnd_int);

  // Draw HAPPY.
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 0.7 - 3.5, 1.7 - 4.0, letters::H.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 1.9 - 3.5, 1.7 - 4.0, letters::A.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 3.1 - 3.5, 1.7 - 4.0, letters::P.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 4.3 - 3.5, 1.7 - 4.0, letters::P.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 5.5 - 3.5, 1.7 - 4.0, letters::Y.to_vec(), rnd_int);

  // Draw RETURNS.
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 0.2 - 3.5, 3.2 - 4.0, letters::R.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 1.4 - 3.5, 3.2 - 4.0, letters::E.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 2.35 - 3.5, 3.2 - 4.0, letters::T.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 3.3 - 3.5, 3.2 - 4.0, letters::U.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 4.4 - 3.5, 3.2 - 4.0, letters::R.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 5.5 - 3.5, 3.2 - 4.0, letters::N.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 6.7 - 3.5, 3.2 - 4.0, letters::S.to_vec(), rnd_int);

  // Draw MARI!
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 1.2 - 3.5, 4.7 - 4.0, letters::M.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 2.4 - 3.5, 4.7 - 4.0, letters::A.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 3.6 - 3.5, 4.7 - 4.0, letters::R.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 4.7 - 3.5, 4.7 - 4.0, letters::I.to_vec(), rnd_int);
  draw_letter(ctx.clone(), vw, vh, ox, oy, scale, 5.7 - 3.5, 4.7 - 4.0, letters::EXC.to_vec(), rnd_int);

  // Finish the highlight over the letterforms.
  ctx.stroke();
}