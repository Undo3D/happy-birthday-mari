!function(
  G, // the window (or global) object
  L, // the name of the library, 'MYALAM'
  N  // the name of the namespace, 'hbm'
){
// Create the MYALAM.hbm namespace, if it does not exist.
G[L]=G[L]||{V:'m1.0.0'},G[L][N]=G[L][N]||{}

seed_to_color = function(seed) {
  let first_char = seed[0];
  if (first_char > 'p') {
    return "#00f".to_string(); // cyan
  } else if (first_char > 'a') {
    return "#f00".to_string(); // magenta
  } else {
    return "#0f0".to_string(); // yellow
  }
}

seed_to_int = function (seed) {
  let second_char = seed[1];
  if (second_char > 'g') {
    return 0; // usually 0, so usually circle
  } else if (second_char > '8') {
    return 1;
  } else {
    return 2;
  }
}

// Define the private draw_small_circle() function.
draw_small_circle = function(
  ctx, vw, vh, x, y
) {
  ctx.moveTo(vw * x, vh * y);
  ctx.arc(vw * (x - 0.01), vh * y, vw * 0.01, 0.0, Math.PI * 2.0);
}

draw_small_star = function (
  ctx, vw, vh, x, y
) {
  ctx.moveTo(vw * (x - 0.02), vh * y);
  ctx.lineTo(vw * (x + 0.02), vh * y);
  ctx.moveTo(vw * (x - 0.01), vh * (y - 0.01));
  ctx.lineTo(vw * (x + 0.01), vh * (y + 0.01));
  ctx.moveTo(vw * (x + 0.01), vh * (y - 0.01));
  ctx.lineTo(vw * (x - 0.01), vh * (y + 0.01));
}

draw_small_triangle = function (
  ctx, vw, vh, x, y
) {
  ctx.moveTo(vw * x, vh * y);
  ctx.lineTo(vw * (x + 0.02), vh * (y + 0.005));
  ctx.lineTo(vw * (x + 0.01), vh * (y + 0.01));
  ctx.lineTo(vw * x, vh * y);
}

// Define the private draw_letter() function.
draw_letter = function(
  ctx, vw, vh, ox, oy, scale, dx, dy, letter, rnd_int
) {
  dx = dx * scale + ox;
  dy = dy * scale + oy;
  for (point of letter) {
    let x = point[0] * scale + dx;
    let y = point[1] * scale + dy;
    if (rnd_int == 0) {
      draw_small_circle(ctx, vw, vh, x, y);
    } else if (rnd_int == 1) {
      draw_small_star(ctx, vw, vh, x, y);
    } else {
      draw_small_triangle(ctx, vw, vh, x, y);
    }
  }
}

// Add the draw_message() function to the MYALAM.hbm namespace.
G[L][N].draw_message = function(vw, vh, canvas_id, start, now, seed) {
  // Convert seed and cycle position to various parameters.
  let cycle_position = (now - start) % 10000.0;
  let base_color = seed_to_color(seed);
  let rnd_int = seed_to_int(seed);
  let line_width = (10000.0 - cycle_position) / 5000.0; // thicker, earlier

  // Get a reference to the canvas’s 2D drawing context.
  let document = G.document;
  let canvas = document.getElementById(canvas_id);
  let ctx = canvas.getContext("2d");
  ctx.beginPath();
  ctx.strokeStyle = base_color;
  ctx.lineWidth = line_width;
  ctx.clearRect(0.0, 0.0, vw, vh);

  // Set the scale of all letters.
  let scale = cycle_position / 40000.0;

  // Set the origin-coordinate of all letters.
  let ox = 0.5;
  let oy = 1.0 - (cycle_position / 10000.0);

  // Draw MANY.
  draw_letter(ctx, vw, vh, ox, oy, scale, 1.2 - 3.5, 0.2 - 4.0, G[L][N].M, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 2.4 - 3.5, 0.2 - 4.0, G[L][N].A, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 3.6 - 3.5, 0.2 - 4.0, G[L][N].N, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 4.8 - 3.5, 0.2 - 4.0, G[L][N].Y, rnd_int);

  // Draw HAPPY.
  draw_letter(ctx, vw, vh, ox, oy, scale, 0.7 - 3.5, 1.7 - 4.0, G[L][N].H, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 1.9 - 3.5, 1.7 - 4.0, G[L][N].A, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 3.1 - 3.5, 1.7 - 4.0, G[L][N].P, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 4.3 - 3.5, 1.7 - 4.0, G[L][N].P, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 5.5 - 3.5, 1.7 - 4.0, G[L][N].Y, rnd_int);

  // Draw RETURNS.
  draw_letter(ctx, vw, vh, ox, oy, scale, 0.2 - 3.5, 3.2 - 4.0, G[L][N].R, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 1.4 - 3.5, 3.2 - 4.0, G[L][N].E, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 2.35 - 3.5, 3.2 - 4.0, G[L][N].T, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 3.3 - 3.5, 3.2 - 4.0, G[L][N].U, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 4.4 - 3.5, 3.2 - 4.0, G[L][N].R, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 5.5 - 3.5, 3.2 - 4.0, G[L][N].N, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 6.7 - 3.5, 3.2 - 4.0, G[L][N].S, rnd_int);

  // Draw MARI!
  draw_letter(ctx, vw, vh, ox, oy, scale, 1.2 - 3.5, 4.7 - 4.0, G[L][N].M, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 2.4 - 3.5, 4.7 - 4.0, G[L][N].A, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 3.6 - 3.5, 4.7 - 4.0, G[L][N].R, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 4.7 - 3.5, 4.7 - 4.0, G[L][N].I, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 5.7 - 3.5, 4.7 - 4.0, G[L][N].EXC, rnd_int);

  // Finish the main letterforms.
  ctx.stroke();

  // Add white highlight, early in the cucle position.
  ctx.strokeStyle = "#fff";
  ctx.lineWidth = line_width / 4.0;

  // Draw MANY.
  draw_letter(ctx, vw, vh, ox, oy, scale, 1.2 - 3.5, 0.2 - 4.0, G[L][N].M, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 2.4 - 3.5, 0.2 - 4.0, G[L][N].A, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 3.6 - 3.5, 0.2 - 4.0, G[L][N].N, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 4.8 - 3.5, 0.2 - 4.0, G[L][N].Y, rnd_int);

  // Draw HAPPY.
  draw_letter(ctx, vw, vh, ox, oy, scale, 0.7 - 3.5, 1.7 - 4.0, G[L][N].H, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 1.9 - 3.5, 1.7 - 4.0, G[L][N].A, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 3.1 - 3.5, 1.7 - 4.0, G[L][N].P, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 4.3 - 3.5, 1.7 - 4.0, G[L][N].P, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 5.5 - 3.5, 1.7 - 4.0, G[L][N].Y, rnd_int);

  // Draw RETURNS.
  draw_letter(ctx, vw, vh, ox, oy, scale, 0.2 - 3.5, 3.2 - 4.0, G[L][N].R, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 1.4 - 3.5, 3.2 - 4.0, G[L][N].E, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 2.35 - 3.5, 3.2 - 4.0, G[L][N].T, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 3.3 - 3.5, 3.2 - 4.0, G[L][N].U, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 4.4 - 3.5, 3.2 - 4.0, G[L][N].R, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 5.5 - 3.5, 3.2 - 4.0, G[L][N].N, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 6.7 - 3.5, 3.2 - 4.0, G[L][N].S, rnd_int);

  // Draw MARI!
  draw_letter(ctx, vw, vh, ox, oy, scale, 1.2 - 3.5, 4.7 - 4.0, G[L][N].M, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 2.4 - 3.5, 4.7 - 4.0, G[L][N].A, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 3.6 - 3.5, 4.7 - 4.0, G[L][N].R, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 4.7 - 3.5, 4.7 - 4.0, G[L][N].I, rnd_int);
  draw_letter(ctx, vw, vh, ox, oy, scale, 5.7 - 3.5, 4.7 - 4.0, G[L][N].EXC, rnd_int);

  // Finish the highlight over the letterforms.
  ctx.stroke();
}
  
}(typeof global=='object'?global:this,'MYALAM','hbm')
