!function(
  G // the window (or global) object
){
  // Define a class which acts a bit like Rust’s `String` type.
  class RustString {
    constructor (str) { this.value = str }
    push_str (str) { this.value += str }
    toString () { return this.value }
    get length () { return this.value.length }
    chars () { return this } // eg for `line.chars().enumerate()`
    collect () { return this }
    count () { return this.value.length }
    enumerate() { return Object.entries(this.value.split('')) }
    parse() { return parseInt(this.value) }
    replace(from, to) {
      this.value = this.value.replace(new RegExp(from,'g'), to)
      return this;
    }
    skip(index) { return new RustString(this.value.slice(index)) }
    slice(from, to) { return new RustString(this.value.slice(from, to)) }
    // slice(from, to) { this.value = this.value.slice(from, to); return this }
    // skip(index) { this.value = this.value.slice(index); return this }
    take(index) { this.value = this.value.slice(0, index); return this }
    // take(index) { return new RustString(this.value.slice(0, index)) }
  }
  // Modify JS’s `String` and `Number` types, so they look a bit more like Rust.
  String.prototype.to_string = function () { return new RustString(this+'') }
  Number.prototype.to_string = function () { return new RustString(this+'') }
  Number.prototype.unwrap = function () { return this }

  // Usage:
  // let foo = "Foo".to_string()
  // console.log('`foo` is ', foo)
  // foo.push_str("Bar")
  // console.log('After `foo.push_str("Bar")`, `foo` is ', foo)
  // console.log(`And stringified, \`foo\` is "${foo}"`)
  // console.log('`foo.chars().enumerate()` is ', foo.chars().enumerate())
  // for (let [p, c] of foo.chars().skip(2).enumerate()) { console.log(p, c) }

}(typeof global=='object'?global:this)
