# Happy Brithday Mari

##### m1.0.0

Birthday greetngs for Mari, 2021

https://undo3d.gitlab.io/happy-birthday-mari/


## License

Standard MIT License. See the ‘LICENSE’ file for details.


## Install build dependencies

1. Install uglify-es 3 globally:  
   `npm install -g uglify-es`
2. Install static-server globally:  
   `npm install -g static-server`
3. Install the Rust toolchain and wasm-pack —  
   see tryout-rust-for-the-first-time.html


## Develop and build

To work on this project, run:  
`node develop.js`

The `develop.js` script will:  
1. Check that your current working directory is the root of the project
2. Copy the `LICENSE` and `VERSION` files from project root to ‘public/’
3. Immediately build ‘src/wasm/main.rs’ to ‘public/lib/wasm/’
4. Immediately build ‘src/js/*.js’ to ‘public/lib/js/’
5. Start watching ‘src/wasm/’ and ‘src/js/’
   — it triggers a build whenever anything changes
6. Start a server on http://localhost:9080/ and open a browser window

There’s no automatic browser refresh when code changes. You’ll need to
manually refresh browser to load changes.